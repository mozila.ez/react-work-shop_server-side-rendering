import React, { Component } from 'react';
import Head from 'next/head';
import Header from './Header';
import Footer from './Footer';
import '../static/css/style.scss';

class Layout extends Component {
    render() {
        const { children, title = "ทดสอบบบ คาเฟ่ บล๊อกเกอร์" } = this.props;
        return (
        <div>
            <Head>
                <title> {title} </title>
                <meta charSet="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
                <link rel="stylesheet" href="/_next/static/style.css" />
            </Head>
            <Header />
            {children}
            <Footer />
        </div>
        )
    }
}

export default Layout;