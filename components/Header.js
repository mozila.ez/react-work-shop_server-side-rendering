import React, { Component } from "react";
import Link from 'next/link';

class Header extends Component {

    constructor(props){
        super(props);
        this.state = {date : new Date()};
        setInterval(() => this.tick(), 1000)

    }

    componentDidMount(){
       this.timerID = setInterval(() => this.tick(), 1000)

    }

    componentDidUpdate(){

    }

    componentWillUnmount(){
        clearInterval(this.timerID);

    }

    tick(){
        this.setState({date : new Date()});
    }
    render(){
        return(
            <div className="container-fluid">
                <div className="d-flex flex-row">
                    <div className="mr-auto col-md-8">
                        <h1 className="text-success">
                            <img style={{height : 70}} src="/static/images/logo/logo.png" al
                            t="" /> {" "} 
                            ทดสอบบบ คาเฟ่{" "}
                        </h1>
                    </div>
                    <div className="ml-auto col-md-4 text-right mt-4">
                        <h5 className="text-muted">
                            {this.state.date.toLocaleTimeString()}
                        </h5>
                        <ul className="list-inline">
                            <li className="list-inline-item title"><Link href="/">หน้าหลัก</Link></li>
                            <li className="list-inline-item title">|</li>
                            <li className="list-inline-item title"><Link href="/about">เกี่ยวกับเรา</Link></li>
                        </ul>
                    </div>
                </div>
                <hr />
            </div>
        )
    }
}

export default Header;